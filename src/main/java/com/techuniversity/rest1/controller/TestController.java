package com.techuniversity.rest1.controller;

import org.springframework.data.repository.query.Param;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

@RestController
@RequestMapping(path = "/test")
public class TestController {

   @GetMapping("/saludos")
   //public String saludar(@RequestParam(value="nombre", requiered = false) String name) --> indicar parametro obligatorio
   public String saludar(@RequestParam(value="nombre", required = false, defaultValue = "TechU") String name){
       return String.format("Saludos %s", name);
   }

   @GetMapping("/varios")
    public String varios(@RequestParam Map<String,String> allParams){
       return "Los parametros son: "+ allParams.entrySet();
   }

   @GetMapping("/multi")
    public String multi(@RequestParam List<String> ids){
       return "Los parametros son :" + ids;
   }

}
